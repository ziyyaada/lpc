const router = require('express').Router();
let Resource = require('../models/resource.model');

router.route('/').get((req, res) => {
    Resource.find()
    .then(resources => res.json(resources))
    .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/add').post((req, res) => {
  const name = req.body.name;
  const description = req.body.description;
  const URL = req.body.URL;
  const rating = Number(req.body.rating);

  const newResource = new Resource({
    name,
    description,
    URL,
    rating
  });

  newResource.save()
  .then(() => res.json('Resource added!'))
  .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/find').get((req, res) => {
  Resource.findById(req.params.id)
    .then(resource => res.json(resource))
    .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/delete').delete((req, res) => {
    Resource.findByIdAndDelete(req.params.id)
    .then(() => res.json('Resource deleted.'))
    .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/update').post((req, res) => {
    Resource.findById(req.params.id)
    .then(resource => {
        resource.name = req.body.name;
        resource.description = req.body.description;
        resource.url = req.body.url;
        resource.rating = Number(req.body.rating);
  
      resource.save()
        .then(() => res.json('Resource updated!'))
        .catch(err => res.status(400).json('Error: ' + err));
    })
    .catch(err => res.status(400).json('Error: ' + err));
});

module.exports = router;