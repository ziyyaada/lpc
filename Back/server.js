const express = require('express');
const mongoose = require('mongoose');
const passport = require('passport');
const flash = require('connect-flash');
const session = require('express-session');
const cors = require('cors');
const app = express();
const port = process.env.PORT || 5000;
const cookieParser = require("cookie-parser");

app.use(cors());
app.use(express.json());
// Passport Config
require('./config/passport')(passport);

// DB Config
const db = require('./config/keys').mongoURI;

// Connect to MongoDB
mongoose
  .connect(
    db,
    { useNewUrlParser: true ,useUnifiedTopology: true, useCreateIndex: true}
  )
  .then(() => console.log('MongoDB Connected'))
  .catch(err => console.log(err));

  app.use(express.urlencoded({ extended: true }));

  // Express session
  app.use(
    session({
      secret: 'secret',
      resave: true,
      saveUninitialized: true
    })
  );

  app.use(flash());

  // Global variables
  app.use(function(req, res, next) {
    res.locals.success_msg = req.flash('success_msg');
    res.locals.error_msg = req.flash('error_msg');
    res.locals.error = req.flash('error');
    next();
  });

const exercisesRouter = require('./routes/exercises');
const usersRouter = require('./routes/users');
const resourcesRouter = require('./routes/resources');

app.use(cookieParser("secretcode"));
app.use(passport.initialize());
app.use(passport.session());

app.use('/exercises', exercisesRouter);
app.use('/users', usersRouter);
app.use('/resources', resourcesRouter);

app.listen(port, () => {
    console.log(`Server is running on port: ${port}`);
});
