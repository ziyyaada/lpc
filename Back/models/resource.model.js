const mongoose = require('mongoose');

const resourceSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    unique: true
  },
  description: {
    type: String,
    required: true
  },
  URL: {
    type: String,
    required: true
  },
  rating: {
      type: Number,
      required: true
  },
  date: {
    type: Date,
    default: Date.now
  }
  
});

const ResourceSchema = mongoose.model('Resource', resourceSchema);

module.exports = ResourceSchema;
