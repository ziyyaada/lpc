const mongoose = require('mongoose');

const user = new mongoose.Schema({
  type: {
    type: String,
    required: true
  },
  name: {
    type: String,
    required: true
   
  },
  email: {
    type: String,
    required: true
  },
  username: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  education: {
    type: String,
    required: true
  },
  date: {
    type: Date,
    default: Date.now
  }
  
});

const User = mongoose.model('User', user);

module.exports = User;
