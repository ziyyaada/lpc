//import to use different modules
import React from 'react';
import ReactDOM from 'react-dom';
//import bootstrap css code
import 'bootstrap/dist/css/bootstrap.min.css';//here first so my css can overwrite bootstrap css
import 'font-awesome/css/font-awesome.min.css';
import 'bootstrap-social/bootstrap-social.css';
import './index.css'; //using the css file
import App from './App';
import * as serviceWorker from './serviceWorker';

//this takes from our App.js and renders it on the html screen
ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  //document is html document. attach App to Dom element
  document.getElementById('root')//imports and uses App
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
