export const RESOURCES =
    [
        {
        id: 0,
        name:'Uthappizza',
        image: '/assets/images/uthappizza.png',
        category: 'https://www.w3schools.com/jsref/jsref_sort.asp',
        label:'Hot',
        price:'4.99',
        featured: true,
        description:'A unique combination of Indian Uthappam (pancake) and Italian pizza, topped with Cerignola olives, ripe vine cherry tomatoes, Vidalia onion, Guntur chillies and Buffalo Paneer.'                    
        },
        {
        id: 1,
        name:'Zucchipakoda',
        image: '/assets/images/zucchipakoda.png',
        category: 'https://www.w3schools.com/jsref/jsref_sort.asp',
        label:'',
        price:'1.99',
        featured: false,
        description:'Deep fried Zucchini coated with mildly spiced Chickpea flour batter accompanied with a sweet-tangy tamarind sauce'
        },
        {
        id: 2,
        name:'Vadonut',
        image: '/assets/images/vadonut.png',
        category: 'https://www.w3schools.com/jsref/jsref_sort.asp',
        label:'New',
        price:'1.99',
        featured: false,
        description:'A quintessential ConFusion experience, is it a vada or is it a donut?'
        },
        {
        id: 3,
        name:'ElaiCheese Cake',
        image: '/assets/images/elaicheesecake.png',
        category: 'https://www.w3schools.com/jsref/jsref_sort.asp',
        label:'',
        price:'2.99',
        featured: false,
        description:'A delectable, semi-sweet New York Style Cheese Cake, with Graham cracker crust and spiced with Indian cardamoms'
        }
    ];