
//make use of views exported by components that we have
//import navbar & NavbarBrand component from reactstrap
import {Navbar, NavbarBrand} from 'reactstrap';
//import the reourceList comonent to be rendered to the UI

import React, { Component } from 'react';
import ResourceList from './search-resources'; //now available
import ResourceDetail from './ResourcedetailComponent';
import AddResources from "./create-resource.component";
import LearningPathList from './LearningPathListComponent'; //now available
import LearningPathDetail from './LearningPathdetailComponent';
import Contact from './ContactComponent';
import Header from './HeaderComponent';
import Footer from './FooterComponent';
import Home from './HomeComponent';
import Login from './login-user';
import {Switch, Route, Redirect, withRouter} from 'react-router-dom';
import axios from 'axios';

//import {connect} from 'react-redux';
//need addComments action creator . so can obtainjs object to put in store
import { postComment, fetchLearningpaths, fetchResources, fetchComments } from '../redux/ActionCreators';

import { render } from '@testing-library/react';



//displaying a page
class Main extends Component {

  constructor(props){
    super(props);
    // alert(this.props.learningpaths.learningpaths.length)
   
    this.state = {//create so can be used in render
      
      searchItem:"",
      lpsearchItem:"",
      resources:[],
      comments:[],
      learningpaths:[]


    
    };
    this.updateSearch = this.updateSearch.bind(this);
  }
  //
  componentDidMount(){
    //import resources and set to state
    axios.get('http://localhost:5000/resources')
    .then(response => {
      this.setState({ resources: response.data })
      
    })
    .catch((error) => {
      console.log(error);
    })
    //import comments from server
    //import learningpaths from server
   
  }

 
  

updateSearch(search){
  
  this.setState({searchItem: search});
  //alert(this.state.searchItem);
  //alert(this.state.resources.length);
}

updateLPSearch(lpsearch){
  
  this.setState({lpsearchItem: lpsearch});
  //alert(this.state.lpsearchItem);
}

  render(){
   
    const HomePage = () =>{
      
      
      return(
        
        //pass in props resources, commments etc
        //to get resources must use reources.resources
        //account for options in 
        <Home resource = {this.state.resources.filter((resource) => resource.featured)[0]}
       
        searchItem = {this.state.searchItem}
        onClick={(searched) => this.updateSearch(searched)}
        
        />
       
       
      );  
      
    }
// what are we passing to the resource detail
//supplying reource and comments as props
//must pass in thord attribute of add comments
    const ResourceWithId = ({match}) =>{
        //alert(match.params.resourceId);
        
      

          return(
            <ResourceDetail resource={this.state.resources.filter((resource)=> resource._id ===
            match.params.resourceId)[0]}
           
            comments ={this.state.comments.filter((comments)=> comments.resourceId
              === parseInt(match.params.resourceId,10))}
              

              //pass the dispatch down. in resource detail is where dispatch happens
              
              postComment={this.props.postComment}
              />

          );
          
    }

    const ResourcePage = () =>{
        return(
          <ResourceList resources ={this.state.resources}
          searchItem = {this.state.searchItem}
          onClick={(searched) => this.updateSearch(searched)}
          comments = {this.state.comments}
          />

        );

    }

    // what are we passing to the resource detail
    const LearningPathWithId = ({match}) =>{
      return(
        <LearningPathDetail learningPath={this.state.learningpaths.filter((learningpath)=> learningpath.id ===
          parseInt(match.params.learningpathId,10))[0]}
        
          comments ={this.state.comments.filter((comments)=> comments.learningpathId
              === parseInt(match.params.learningpathId,10))}
          //ErrMess = {this.props.comments.errMess}
          />
          

      );
}

const LearningPathPage = () =>{
  return(
    <LearningPathList learningpaths ={this.state.learningpaths}
    
    lpsearchItem = {this.state.lpsearchItem}
    onClick={(searched) => this.updateLPSearch(searched)}
    comments = {this.state.comments}
    />

  );

}


  return (
    <div>
      < Header lpsearchItem = {this.state.lpsearchItem}
            searchItem = {this.state.searchItem}
            onClicklpSearch = {(searched) => this.updateLPSearch(searched)}
            onClickSearch = {(searched) => this.updateSearch(searched)} />
      <Switch>
        <Route path="/home" component={HomePage}/>
        {/* creating function component resourceList, exact path for resources */}
        <Route exact path="/resources" component={ResourcePage}/>
        <Route exact path="/resources/create" component={AddResources}/>
       <Route path ="/resources/:resourceId" component={ResourceWithId}/>
       
       <Route exact path="/learningpaths" component={LearningPathPage}/>
       <Route path ="/learningpaths/:learningpathId" component={LearningPathWithId}/>
      <Route path = "/login" component={Login}/>
       <Route exact path="/contactus" component = {Contact}/>
       <Redirect to="/home"/>
      </Switch>
      <Footer/>
    </div>
  );
  }
}


export default (Main);