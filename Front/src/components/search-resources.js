import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import {Button} from "@material-ui/core";
const Resource = props => (
  <tr>
    <td>{props.resource.name}</td>
    <td>{props.resource.description}</td>
    <td>{props.resource.URL}</td>
    <td>{props.resource.rating}</td>
  </tr>
)

export default class ResourceList extends Component {
  constructor(props) {
    super(props);
    this.onSearch = this.onSearch.bind(this);
    this.buttonSearch = this.buttonSearch.bind(this);
    this.state = {name: '', search:'',resources: []};
  }

  componentDidMount() {
    axios.get('http://localhost:5000/resources')
      .then(response => {
        this.setState({ resources: response.data })
      })
      .catch((error) => {
        console.log(error);
      })
  }

  deleteResource(id) {
    axios.delete('http://localhost:5000/resources/delete')
      .then(response => { console.log(response.data)});

    this.setState({
        resources: this.state.resources.filter(el => el._id !== id)
    })
  }

  onSearch(e){
    this.setState({
      search : e.target.value
    })
  }
  buttonSearch(){
    this.setState({search : this.state.search})
    
  }
  render() {
    let filteredResources = this.state.resources.filter(
      (resource) =>{
        return resource.name.toLowerCase().indexOf(this.state.search.toLowerCase()) !==-1;
      }
    );
    
    return (
      <div>
        <div className="col-12 col-md-12">
          <form>
            <div class="form-group row">
              <div class="col-md-10">
                <input
                   type ="text" class="form-control" id="search" name="search" placeholder="Search"
                  onChange={this.onSearch}
                />
                </div>
                <Button color="primary" variant="outlined" onClick={this.buttonSearch}>Search</Button>
              </div>
              
            </form>
            </div>
        <h3>Logged Resources</h3>
        <table className="table">
          <thead className="thead-light">
            <tr>
              <th>Name</th>
              <th>Description</th>
              <th>URL</th>
              <th>Rating</th>
            </tr>
          </thead>
          <tbody>
            { filteredResources.map((resource)=>{
              return <Resource resource ={resource}
              key = {resource._id}/>
            })}
          </tbody>
        </table>
      </div>
    )
  }
}