import React, { Component } from 'react';


export default class Quiz extends Component {
    state = {questionNo:"hello"}
    constructor(props){
        super(props);
            this.onChange = this.onChange.bind(this);
            this.onProgress = this.onProgress.bind(this);
            this.onStart = this.onStart.bind(this);
            this.state = {
                mark: 0,
                questions: ["First question", "Second question", "Third Question", "Fourth Question", "Fifth Question", "Sixth Question"],
                questionNo: 0,
                answers: [0,0,0,0,0],
                progress: 0,
                dis: true
            }
        }
   
    onChange(e){
        this.setState({
            questionNo:e.target.value
        });
    }
    
    onProgress(e){
        this.setState({
            progress: e.target.value
        })
    }
    onStart(){
        this.setState({
            dis: false
        })
    }
  render() {
    const {questionNo} = this.state;
    const {progress} = this.state;
    const {dis} = this.state;
    const {questions} = this.state;
    return (
   
        <form>
        <h1>Question: {progress}</h1>
          <div className="form-group">
            
                <input type = "radio"
                       value = "1"
                       checked = {questionNo==="1"}
                       onChange={this.onChange}/>
                    <label>
                    {questions[0]}
                    </label>
                    <br/>
            
                <input type = "radio"
                       value = "2"
                       checked={questionNo==="2"}
                       onChange={this.onChange}/>
                       <label>
                       {questions[1]}
            </label><br/>
            
                <input type = "radio"
                       value = "3"
                       checked={questionNo==="3"}
                       onChange={this.onChange}/>
                       <label>
                       {questions[2]}
            </label><br/>
            
                <input type = "radio"
                       value = "4"
                       checked={questionNo==="4"}
                       onChange={this.onChange}/>
                       <label>
                       {questions[3]}
            </label><br/>
            
                <input type = "radio"
                       value = "5"
                       checked={questionNo==="5"}
                       onChange={this.onChange}/>
                       <label>
                       {questions[4]}
            </label>  
          </div>
          <progress id="progress" value={progress} max="100"></progress>
                      <label id="progressLabel">{progress}%</label><br/>
                      <button type = "button" onClick={this.onStart}>Start</button>
                      <button disabled={dis} type ="button" value = {parseInt(progress)-20} onClick={this.onProgress}>Previous</button> 
                      <button disabled={dis} type = "button" value = {parseInt(progress)+20} onClick={this.onProgress}>Next</button>  
                      <button disabled={dis} type = "button" >Submit</button>
          <div className="form-group">
            <input type="submit" value="Create User" className="btn btn-primary" />
          </div>
        </form>
 
    )
  }
}