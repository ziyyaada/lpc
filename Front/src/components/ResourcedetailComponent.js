import React, { Component } from 'react';

import { Control, LocalForm, Errors } from 'react-redux-form';
import { Card, CardImg, CardText, CardBody,
    CardTitle, Breadcrumb, BreadcrumbItem, Button, Modal, ModalHeader, ModalBody, Row, Col, Label } from 'reactstrap';
import { Link } from 'react-router-dom';
import {Loading } from './LoadingComponent'
import {baseUrl} from '../shared/baseUrl';
//displaying a page


const required = (val) => val && val.length; //receives value as parameter and checks value bigger than 0
const maxLength = (len) => (val) => !(val) || (val.length <= len); //takes length and returns fn of fn checks if has no value
//checsks len is less than param value
const minLength = (len) => (val) => val && (val.length >= len); //set min length. check greater than 0 and less than max
//const isNumber = (val) => !isNaN(Number(val));
const isRated = (val) => val &&val.length;


class CommentForm extends Component{

    constructor(props){
        super(props);
        this.toggleModal = this.toggleModal.bind(this);   
        this.handleSubmit = this.handleSubmit.bind(this);
        this.viewResource = this.viewResource.bind(this);
        this.state = {
            isModalOpen: false //set as not open. will track the modal
        };
    }

    //track the state of the modal and change it to open/closed
    toggleModal() {
        this.setState({
          isModalOpen: !this.state.isModalOpen
        });
      } 
      // replace pop up alert by sending back data 
      handleSubmit(values) {
          this.toggleModal();
        //receiives the parameter values
       // alert()
       
        console.log('Current State is: ' + JSON.stringify(values));
       // alert('Current State is: ' + JSON.stringify(values));
        //alert("this is the rating ffrom form: " + values.rating + this.props.resourceId)
        //when submit comments then the comment will be added to the comemnts
        this.props.postComment(this.props.resourceId, values.rating,
            values.author, values.comment);
        //alert(values.author +values.comment);    
        // event.preventDefault();
    }

     viewResource(resource){
        //  alert(resource);
        //  alert(this.props.resource);
        window.open(resource.category);
    }
    
    render(){

        return(
            <container>

            {/* <Button outline onClick={this.viewResource(this.props.resource)}><span className="fa fa-sign-in fa-lg"></span>View Resource</Button>     */}
            <Button outline onClick={this.toggleModal}><span className="fa fa-sign-in fa-lg"></span>Submit Comment</Button>
            
            
                            {/* <button className="btn btn-success col-form-button " onClick={()=>
                            this.viewResource(this.props.resource)}>View Resource</button> */}
                           

            <Modal isOpen={this.state.isModalOpen} toggle={this.toggleModal}>

                    {/* MODAL STARTS HERE */}
                    <ModalHeader toggle={this.toggleModal}>Submit Comment</ModalHeader> 
                        <ModalBody>
                        <LocalForm onSubmit={(values) => this.handleSubmit(values)}>
                            {/* form groups become rows */}
                            <Row className="form-group">
                                <Label htmlFor="rating" md={0}></Label>
                                <Col md={12}> Rating              
                                    <Control.select model=".rating" id="rating" name="rating"
                                        placeholder="Rating"
                                        className="form-control"
                                        // USE VALIDATORS APPLIED
                                        validators={{
                                            isRated
                                        }}
                                         >
                                             <option>Select Rating</option>
                                             <option >1</option>
                                             <option >2</option>
                                             <option >3</option>
                                             <option >4</option>
                                             <option >5</option>
                                         </Control.select>
                                         {/* CAN USE errors to make use of what want to display */}
                                    <Errors
                                       // className="text-danger"
                                        model=".rating"
                                        show="touched"// only show if message is selected
                                        messages={{//these are the messages to be displayed
                                            isRated: 'Required'      
                                        }}
                                     />
                                </Col>
                            </Row>
                            <Row className="form-group">
                                <Label htmlFor="author" md={0}></Label>
                                <Col md={12}> Your Name
                                    <Control.text model=".author" id="author" name="author"
                                        placeholder="Your Name"
                                        className="form-control"
                                        validators={{
                                            required, minLength: minLength(3), maxLength: maxLength(15)
                                        }}
                                         />
                                    <Errors
                                        className="text-danger"
                                        model=".author"
                                        show="touched"
                                        messages={{
                                            required: 'Required',
                                            minLength: 'Must be greater than 3 characters',
                                            maxLength: 'Must be 15 characters or less'
                                        }}
                                     />
                                </Col>
                            </Row>                
                            <Row className="form-group">
                                <Label htmlFor="comment" md={0}></Label>
                                <Col md={12}> Comment
                                    <Control.textarea model=".comment" id="comment" name="comment"
                                        rows="6"
                                        className="form-control" />
                                </Col>
                            </Row>
                            <Row className="form-group">
                                <Col md={{size:4, offset: 0}}>
                                    <Button type="submit" color="primary">
                                    Send Feedback
                                    </Button>
                                </Col>
                            </Row>
                        </LocalForm>                   
                    </ModalBody>
                </Modal>
            </container>
        );

    };


}

//--------------------COMMENTFORM COMPONENT END---------------------------
 //user defined components start with captals
//resource is props object so must be in curly brackets
    function RenderResource({resource}){
        console.log('Resource Dteail component Render being invoked');

        
        if(resource != null) //if it is a resource we will draw the followin
            
            return(
                
                    <div  className="col-12 col-md-5 m-1"> 
                      {/* <Card> */}
                          {/* getting image from server */}
                        {/* <CardImg top src= {baseUrl + resource.image} alt={resource.name}/>    */}
                        {/* <CardBody> */}
                                {/* <CardTitle heading>{resource.name}</CardTitle>             */}
                                {/* <CardText>{resource.description}</CardText> */}
                                
                        {/* </CardBody>    */}
                      
                      {/* </Card>   */}
                    </div>
                
                 );        
        else
            return (
                <div></div>//return empty div so nothing made
            );
    } 
    //will receive comments, add comments and resource Id.
    //these must be passed to the commetn form
    function RenderComments({comments, postComment, resourceId, resource}){
        // alert(comments.length);
       // comments.sort(function(a,b){return b.id - a.id});
       // console.log("These are the comments "+comments.length());
       if(comments != null) //if it is a resource we will draw the followin     
            return(
            <div  className="col-12 col-md-5 m-1"> 
                    <h4> Comments</h4>                 
                    <ul className ="list-unstyled">
                         {comments.map((comment) => {// map iterates over them
            //whatever returned from this function will be used in the return from render
                        
                        return(
                            <li key={comment.id}>
                            <p>{comment.comment}</p>
                                <p>-- {comment.author} , {new Intl.DateTimeFormat('en-US', {year: 'numeric', month: 'short', day: '2-digit'}).format(new Date(Date.parse(comment.date)))}</p>   
                            </li>
                        );                       
                    })}
                    </ul>  
                    {/* need to pass props down to the comment form for use */}
                    <CommentForm resourceId = {resourceId} 
                    postComment={postComment}
                    resource = {resource}
                    />
                 </div>    
            );                                 
        else        
            return (
                 <div></div>
             );        
    }
   
    function viewResource(resource){
        //  alert(resource);
        //  alert(this.props.resource);
        window.open(resource.category);
    }

    const Resourcedetail = (props)=>{
            //resource props might not have info so must accoutn for that
         //alert(props.resource);
          
         //can display reources
        if(props.resource != null)
        return (  

            <div class='container'>
                {/* put breadcrumb here */}
                <div className="row">
                    <Breadcrumb>
                        {/* go back to resources from here */}
                        <BreadcrumbItem><Link to="/resources">Resources</Link></BreadcrumbItem>
                        <BreadcrumbItem active>{props.resource.name}</BreadcrumbItem>
                    </Breadcrumb>
                    <div className="col-12">
                        {/* title on page */}
                        <h3>{props.resource.name}</h3>
                        <p>{props.resource.description}</p>
                        <button className="btn btn-success col-form-button " onClick={()=>
                            {viewResource(props.resource)}}>View Resource</button>
                        <hr />
                    </div>                
                </div>


                <div className = "row">
                {/* //   call this to draw on page */}

                     {/* <RenderResource resource={props.resource}/> */}
                     {/* will need to dispatch back here */}
                     <RenderComments comments={props.comments}
                     postComment={props.postComment}
                     //will need resourceId here
                     resourceId={props.resource.id}
                     resource={props.resource}
                     />
                </div>    

            </div>
        );
        else
            return(
                <div>nothing there</div>
        );         
    } 
  //exporting this so can be used in other files
export default Resourcedetail;