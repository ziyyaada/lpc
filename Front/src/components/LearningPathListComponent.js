import React from 'react';
import { Card, CardImg, CardImgOverlay,
    CardTitle, Breadcrumb,BreadcrumbItem, Table } from 'reactstrap';
import {Link} from 'react-router-dom';   
import {Loading} from './LoadingComponent'; 


    const LearningPathList = (props) => {


        function rate({learningPath}){
            //alert(learningPath.id);
            var rating =0;
            var count =1;
                {props.comments.comments.map((comment) => {
                    if(comment.learningPathId === learningPath.id){
                        rating += parseInt(comment.rating)
                        count++;
                        //alert(rating);
                     }else {
                         rating= 0;
                         count=2;
                     }
                    
                })}
                return (
                    (rating/(count-1) * 10) + "%"
                );         
        } 

        function renderMenuItem (learningPath) {
            return (
    
    
                <tr key={learningPath.id}
                >
            
                {/* // <th scope="row col-12 col-md-5 m-1" >ddd</th> */}
             <td> {learningPath.name} <br></br><br/> {learningPath.description} <br></br> <a href="www.here" target="_blank">{learningPath.price}</a> </td>  
             {/* <td>        </td> */}
             <td >{rate({learningPath})}   </td>
            <td>< Link to={`/learningpaths/${learningPath.id}`} >
                            <button className="btn btn-success col-form-button " onClick={()=>
                            viewlearningPath(learningPath)}>View learningPath</button>
                            </Link></td>
        
            </tr>
    
            );
        }
        
        function viewlearningPath(learningPath){
           // window.open(learningPath.category);
        }

        function setLPSearch(){
            //alert('Hello');
            var lpsearch =document.getElementById("search").value;
            props.onClick(lpsearch);
            
            //alert(search);
            
        }  
          
           var found =false; 

           function learningPathList(){
                                        // alert(props.learningpaths.learningpaths.length);
                                    const tests = props.learningpaths.filter((learningpaths)=> learningpaths.name.toLowerCase().includes(props.lpsearchItem) 
                                    ||  learningpaths.description.toLowerCase().includes(props.lpsearchItem)   
                                    )
                                    //alert(tests.length);
                                    if(tests.length<1){

                                        return(
                                        <div className="row col-12">    
                                        <div>There were no matches for your search, please search for another topic. </div>
                                    </div>
                                        );
                                    }
                                    else{

                                        return(
                                            <div className = "row">
                                            <table className= "table-striped "  id = "myTable">
                                            <thead>
                                                <tr>
                                                
                                                    <th scope="col"><h5>Description</h5></th>
                                                    
                                                    <th scope="col =3"><h5>Rating</h5></th>
                                                    
                                                </tr>
                                            </thead>
                                            <tbody className = "col-12 col-md-12">
                                                {props.learningpaths.learningpaths.map((learningPath) => {
                                                    props.lpsearchItem.toLowerCase();
                                    
                                                if(props.lpsearchItem === ""){
                                                    found = true;
                                    
                                                return (
                                        
                                                    <>    {renderMenuItem (learningPath)}</>                    
                                                );
                                                }

                                                else if(learningPath.name.toLowerCase().includes(props.lpsearchItem)||learningPath.description.toLowerCase().includes(props.lpsearchItem)){
                                                    found=true;
                                                        return(
                            
                                                        <>   {renderMenuItem (learningPath)} </> 
                                
                                                            
                                                            );
                                                }     
                                            })}
                                            </tbody>
                                        </table>   
                                        <hr/>
                                    </div>     
                                    
                                        );     
                                    }         
             }


                                                       

        
        //    ---------------------Learning Paths draw------------------------
        return (
            <div className="container">

                <br></br>
          

                <div className="row">
                    <Breadcrumb>
                        <BreadcrumbItem><Link to="/home">Home</Link></BreadcrumbItem>
                        <BreadcrumbItem active>Learning Paths</BreadcrumbItem>
                    </Breadcrumb>
                    <div className="col-12">
                        <h3>Learning Paths</h3>
                        {/* add line under heading */}
                        <hr />

                         {/* Search Bar Here */}
                <div className = "col-12 col-md-12">
                    <form>
                        <div class="form-group row">
                            <div class="col-md-10">
                                <input type ="text" class="form-control" id="search" name="search" placeholder="Search"/>
                            </div>
                            < Link to='/LearningPaths' className="col-md-2">
                            <button className="btn btn-success col-form-button col-md-12 " onClick={()=>
                            setLPSearch()}>Search</button>
                            </Link>
                            {/* <a role ="button" id="searchBut" class="btn btn-success col-md-2 col-form-button">Search</a> */}
                        </div>
                     </form>

                </div>
                <hr />
            {/* search bar ends */}
                    </div>                
                </div>
                <div className="row col-12 col-md-12 m-1">
                    {learningPathList()}
                    
                </div>
            </div>
        );
    }        

export default LearningPathList;