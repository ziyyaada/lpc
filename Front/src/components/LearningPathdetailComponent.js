import React from 'react';
import { Card, CardImg, CardText, CardBody,
    CardTitle, Breadcrumb, BreadcrumbItem, Button } from 'reactstrap';
import { Link } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import StepContent from '@material-ui/core/StepContent';
// import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
//displaying a page


const useStyles = makeStyles((theme) => ({
    root: {
      width: '100%',
    },
    button: {
      marginTop: theme.spacing(1),
      marginRight: theme.spacing(1),
    },
    actionsContainer: {
      marginBottom: theme.spacing(2),
    },
    resetContainer: {
      padding: theme.spacing(3),
    },
  }));
  
  function getSteps(comments) {
    //    alert(comments.length);
    var labels = [];
    // for(var i =0;i<comments.length;i++ ){

    // }
    return ['Select campaign settings','Select campaign settings', 'Create an ad group',  'Create an ad', 'extra']
    // ,'Select campaign settings', 'Create an ad group', 'Create an ad', 'extra'];
  }

  function VerticalLinearStepper(props) {
    const classes = useStyles();
    const [activeStep, setActiveStep] = React.useState(0);
    const steps = getSteps(props.learningPath);
  
    const handleNext = () => {
      setActiveStep((prevActiveStep) => prevActiveStep + 1);
    };
  
    const handleBack = () => {
      setActiveStep((prevActiveStep) => prevActiveStep - 1);
    };
  
    const handleReset = () => {
      setActiveStep(0);
    };

    function getStepContent(step) {
        switch (step) {
          case 0:
            return `For each ad campaign that you create, you can control how much
                    you're willing to spend on clicks and conversions, which networks
                    and geographical locations you want your ads to show on, and more.`;
          case 1:
            return props.learningPath.id;
          case 2:
            return `Try out different ad text to see what brings in the most customers,
                    and learn how to enhance your ads using features like ad extensions.
                    If you run into any problems with your ads, find out how to tell if
                    they're running and how to resolve approval issues.`;
          default:
            return 'Bonus Material!';
        }
      }
  
    return (
      <div className={classes.root} >
        <Stepper activeStep={activeStep} orientation="vertical" className="col-12">
          {steps.map((label, index) => (
            <Step key={label}>
              <StepLabel><h3>{label}</h3></StepLabel>
              <StepContent>
                  {/* add text here */}

                {/* <Typography>{props.learningPath.description}</Typography> */}
                <Typography>{getStepContent(index)}</Typography>
                <div className={classes.actionsContainer}>
                  <div>
                    <Button
                      disabled={activeStep === 0}
                      onClick={handleBack}
                      className={classes.button}
                    >
                      Back
                    </Button>
                    <Button
                      variant="contained"
                      color="primary"
                      onClick={handleNext}
                      className={classes.button}
                    >
                      {activeStep === steps.length - 1 ? 'Finish' : 'Next'}
                    </Button>
                  </div>
                </div>
              </StepContent>
            </Step>
          ))}
        </Stepper>
        <hr />
        {activeStep === steps.length && (
          <Paper square elevation={0} className={classes.resetContainer}>
            <Typography>All steps completed - you&apos;re finished</Typography>
            <Button onClick={handleReset} className={classes.button}>
              Reset
            </Button>
          </Paper>
        )}
      </div>
    );
  }

    const LearningPathDetail = (props)=>{
        if(props.learningPath != null)
        return (  

            <div class='container'>
                {/* put breadcrumb here */}
                <div className="row">
                    <Breadcrumb>
                        {/* go back to learningpathss from here */}
                        <BreadcrumbItem><Link to="/learningpaths">Learning Paths</Link></BreadcrumbItem>
                        <BreadcrumbItem active>{props.learningPath.name}</BreadcrumbItem>
                    </Breadcrumb>
                    
                    <div className="col-12">
                    <hr />
                        {/* title on page */}
                        <h3>{props.learningPath.name}</h3>
                        <h7>{props.learningPath.description}</h7>
                        <hr />
                    </div>                
                </div>
                {/* <div className = "row"> */}
                {/* //   call this to draw on page */}
          {VerticalLinearStepper(props)}
            </div>
        );
        else
            return(
                <div></div>
        );        
    } 
  //exporting this so can be used in other files
export default LearningPathDetail;