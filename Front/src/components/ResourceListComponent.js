import React from 'react';
import { Card, CardImg, CardImgOverlay,
    CardTitle, Breadcrumb,BreadcrumbItem, Table } from 'reactstrap';
import {Link} from 'react-router-dom';    
import {Loading} from './LoadingComponent';
import {Button} from "@material-ui/core";

    const ResourceList = (props) => {
        

        function rate({resource}){
            //alert(resource.id);
            /*var rating =0;
            var count =1;
                {props.comments.map((comment) => {
                    if(comment.resourceId === resource.id){
                        rating += parseInt(comment.rating)
                        count++;
                        //alert(rating);
                     }
                    
                })}*/
                return (
                    resource.rating+"/10"
                );         
        } 

        function renderMenuItem (resource) {
            return (
    
    
                <tr key={resource._id}
                >
            
                {/* // <th scope="row col-12 col-md-5 m-1" >ddd</th> */}
             <td><div style={{"fontFamily":"Constantia","fontSize":"x-large","fontStyle":"italic","fontWeight":"1000"}}> {resource.name} </div><br></br><br/> {resource.description} <br></br> <a href="www.here" target="_blank">{resource.price}</a> </td>  
             <td>        </td>
             <td >{rate({resource})}  </td>
             {/* <td>  </td> */}
            <td>< Link to={`/resources/${resource._id}`} >
                            <Button color="secondary" variant="outlined" onClick={()=>
                            viewResource(resource)}>View Resource</Button>
                            </Link></td>
        
            </tr>
    
            );
        }
        
        function viewResource(resource){
            //window.open(resource.category);
        }

        function setSearch(){
            //alert('Hello');
            var search =document.getElementById("search").value;
            props.onClick(search);
            
            //alert(search);
            
        }  
          
           var found =false; 

           function resourceList(){

                                    const tests = props.resources.filter((resources)=> resources.name.toLowerCase().includes(props.searchItem) 
                                    ||  resources.description.toLowerCase().includes(props.searchItem)   
                                    )
                                    //alert(tests.length);
                                    if(tests.length<1){

                                        return(
                                        <div className="row col-12">    
                                        <div>There were no matches for your search, please search for another topic. </div>
                                    </div>
                                        );
                                    }
                                    else{

                                        return(
                                            <div className = "row">
                                            <table className= "table-striped "  id = "myTable">
                                            <thead>
                                                <tr>
                                                
                                                    <th scope="col"><h5>Description</h5></th>
                                                    <th></th>
                                                    <th scope="col =3"><h5>Rating</h5></th>
                                                    
                                                </tr>
                                            </thead>
                                            <tbody className = "col-12 col-md-12">
                                                {props.resources.map((resource) => {
                                                    props.searchItem.toLowerCase();
                                    
                                                if(props.searchItem === ""){
                                                    found = true;
                                    
                                                return (
                                        
                                                    <>    {renderMenuItem (resource)}</>                    
                                                );
                                                }

                                                else if(resource.name.toLowerCase().includes(props.searchItem)||resource.description.toLowerCase().includes(props.searchItem)){
                                                    found=true;
                                                        return(
                            
                                                        <>   {renderMenuItem (resource)} </> 
                                
                                                            
                                                            );
                                                }     
                                            })}
                                            </tbody>
                                        </table>   
                                        <hr/>
                                    </div>     
                                    
                                        );     
                                    }         
                                }
        
        //    ---------------------resources draw------------------------
            if(props.resources.isLoading){
                 return(
                         <div className="container">
                              <div classname="row">
                               {/* display spinner */}
                                  <Loading/>
                              </div>
                         </div>
                        );
             } 
            else if(props.resources.errMess){
                        
                 return(
                         <div className="container">
                                 <div classname="row">
                                     {/* display error message */}
                                     <h4>{props.resources.errMess}</h4>
                                 </div>
                         </div>
                        );
                        
             } 
                                
        else
            return (
                <div className="container">

                    <br></br>
            

                    <div className="row">
                        <Breadcrumb>
                            <BreadcrumbItem><Link to="/home">Home</Link></BreadcrumbItem>
                            <BreadcrumbItem active>Resources</BreadcrumbItem>
                        </Breadcrumb>
                        <div className="col-12">
                            <h3>Resources</h3>
                            {/* add line under heading */}
                            <hr />

                            {/* Search Bar Here */}
                    <div className = "col-12 col-md-12">
                        <form>
                            <div class="form-group row">
                                <div class="col-md-10">
                                    <input type ="text" class="form-control" id="search" name="search" placeholder="Search" />
                                </div>
                                < Link to='/resources' className="col-md-2">
                                <button className="btn btn-success col-form-button col-md-12 " onClick={()=>
                                setSearch()}>Search</button>
                                </Link>
                                {/* <a role ="button" id="searchBut" class="btn btn-success col-md-2 col-form-button">Search</a> */}
                            </div>
                        </form>

                    </div>
                    <hr />
                {/* search bar ends */}
                        </div>                
                    </div>
                    <div className="row col-12 col-md-12 m-1">
                        {resourceList()}
                        
                    </div>
                </div>
            );
        }        

export default ResourceList;