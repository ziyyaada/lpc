import React, { Component } from 'react';
import axios from 'axios';
import "react-datepicker/dist/react-datepicker.css";
import { Card, CardImg, CardImgOverlay,
  CardTitle, Breadcrumb,BreadcrumbItem, Table } from 'reactstrap';
import {Link} from 'react-router-dom';      

export default class Createresource extends Component {
  constructor(props) {
    super(props);

    this.onChangeName = this.onChangeName.bind(this);
    this.onChangeDescription = this.onChangeDescription.bind(this);
    this.onChangeURL = this.onChangeURL.bind(this);
    this.onChangeRating = this.onChangeRating.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

    this.state = {
      name: '',
      description: '',
      URL:'',
      rating: 0,
      resources: []
    }
  }


  onChangeName(e) {
    this.setState({
      name: e.target.value
    })
  }

  onChangeDescription(e) {
    this.setState({
      description: e.target.value
    })
  }

  onChangeURL(e) {
    this.setState({
      URL: e.target.value
    })
  }

  onChangeRating(e) {
    this.setState({
      rating: e.target.value
    })
  }

  onSubmit(e) {
    e.preventDefault();

    const resource = {
      name: this.state.name,
      description: this.state.description,
      URL: this.state.URL,
      rating: this.state.rating
    }

    console.log(resource);

    axios.post('http://localhost:5000/resources/add/', resource)
      .then(res => console.log(res.data));

    window.location = '/';
  }

  render() {
    return (
      <div class='container'>
        <br></br>
        <div className="row">
             <Breadcrumb>
                <BreadcrumbItem><Link to="/home">Home</Link></BreadcrumbItem>
                <BreadcrumbItem active>Upload</BreadcrumbItem>
              </Breadcrumb>
              <div className="col-12">
                            <h3>Upload a Resource</h3>
                            {/* add line under heading */}
                            <hr />
              </div>
    
        </div>

      <form onSubmit={this.onSubmit}>
        <div className="form-group"> 
          <label>Name: </label>
          <input 
              type="text" 
              required
              className="form-control"
              value={this.state.name}
              onChange={this.onChangeName}
              />
        </div>
        <div className="form-group"> 
          <label>Description: </label>
          <input  type="text"
              required
              className="form-control"
              value={this.state.description}
              onChange={this.onChangeDescription}
              />
        </div>
        <div className="form-group">
          <label>URL: </label>
          <input 
              type="text" 
              className="form-control"
              value={this.state.URL}
              onChange={this.onChangeURL}
              />
        </div>
        <div className="form-group">
          <label>Rating: </label>
          <input 
              type="text" 
              className="form-control"
              value={this.state.rating}
              onChange={this.onChangeRating}
              />
        </div>
        <hr />
        <div className="form-group">
          <input type="submit" value="Upload Resource" className="btn btn-primary" />
        </div>
      </form>
      <hr />
    </div>
    )
  }
}