import React, { Component } from 'react';
import axios from 'axios';
import {Button} from "@material-ui/core";
import 'rxjs/add/operator/concatMap';
export default class LoginRegister extends Component {
    constructor(props) {
      super(props);
      this.state = {isLoginOpen:true, isRegisterOpen: false};
    }

    showLoginBox() {
        this.setState({isLoginOpen: true, isRegisterOpen: false});
      }
    
      showRegisterBox() {
        this.setState({isRegisterOpen: true, isLoginOpen: false});
      }
    render() {
        return (
            <div className="root-container">
                <div className="box-controller">
       <div
         className={"controller " + (this.state.isLoginOpen
         ? "selected-controller"
         : "")}
         onClick={this
         .showLoginBox
         .bind(this)}>
         Login
       </div>
       <div
         className={"controller " + (this.state.isRegisterOpen
         ? "selected-controller"
         : "")}
         onClick={this
         .showRegisterBox
         .bind(this)}>
         Register
       </div>
     </div>
            <div className="box-container">
            {this.state.isLoginOpen && <LoginBox/>}
            {this.state.isRegisterOpen && <RegisterBox/>}
            </div>
            </div>
          );
    }
  
  }

  class LoginBox extends Component{
    constructor(props) {
        super(props);
        this.onUsername = this.onUsername.bind(this);
        this.onPass = this.onPass.bind(this);
        this.state={
          username:'',
          password:''
        }
      }
    
      submitLogin(e) { e.preventDefault();
  
        const login = {
          username: this.state.username,
          password:this.state.password,    
        }
    
        console.log(login);
    
        axios.post('http://localhost:5000/users/login/', login)
          .then(res => console.log(res.data));
    }
    onUsername(e) {
      this.setState({
        username: e.target.value
      })
    }
   
    onPass(e){
        this.setState({
            password: e.target.value
        })
    }
      render() {
        return (
          <div className="inner-container">
            <div className="header">
              Login
            </div>
            <div className="box">
    
              <div className="input-group">
                <label htmlFor="username">Username</label>
                <input
                  type="text"
                  name="username"
                  className="login-input"
                  placeholder="Username"
                  onChange={this.onUsername}/>
              </div>
    
              <div className="input-group">
                <label htmlFor="password">Password</label>
                <input
                  type="password"
                  name="password"
                  className="login-input"
                  placeholder="Password"
                  onChange={this.onPass}>

                  </input>
              </div>
    
              <button
                type="button"
                className="login-btn"
                onClick={this
                .submitLogin
                .bind(this)}>Login</button>
            </div>
          </div>
        );
      }
  }
class RegisterBox extends Component {

    constructor(props) {
      super(props);

    this.onType = this.onType.bind(this);
    this.onName = this.onName.bind(this);
    this.onEmail = this.onEmail.bind(this);
    this.onChangeUsername = this.onChangeUsername.bind(this);
    this.onPassword = this.onPassword.bind(this);
    this.onEducation = this.onEducation.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

      this.state = {
        type: '',
        name: '',
        email:'',
        username: '',
        password:'',
        education:''
    }
    }
    onType(e){
        this.setState({
            type: e.target.value
        })
    }
  
    onName(e){
      this.setState({
          name:e.target.value
      })
      }
      
      onEmail(e){
          this.setState({
              email:e.target.value
          })
      }
  
    onChangeUsername(e) {
      this.setState({
        username: e.target.value
      })
    }
   
    onPassword(e){
        this.setState({
            password: e.target.value
        })
    }
    onEducation(e){
        this.setState({
            education:e.target.value
        })
    }
  
    onSubmit(e) {
      e.preventDefault();
  
      const register = {
          type:this.state.type,
          name:this.state.name,
          email:this.state.email,
        username: this.state.username,
        password:this.state.password,
        education:this.state.education
  
      }
  
      console.log(register);
  
      axios.post('http://localhost:5000/users/add/', register)
        .then(res => console.log(res.data));
      
    }
  
    render() {
      return (
        <div className="inner-container">
          <div className="header">
            Register
          </div>
          <div className="box">
  
            <div className="form-group">
            <label>Choose:</label>
                    <input ref="text"
                    required
                    className="form-control"
                    value={this.state.type}
                    onChange={this.onType}>
                </input>
            </div>
  
            <div className="form-group">
            <label>Name: </label>
            <input ref="text"
                required
                className="form-control"
                value={this.state.name}
                onChange={this.onName}>
            </input>
            </div>

            <div className="form-group">
            <label>Email: </label>
            <input ref="text"
                required
                className="form-control"
                value={this.state.email}
                onChange={this.onEmail}>
            </input>
            </div>
            <div className="form-group">
            <label>Username: </label>
            <input ref="text"
                required
                className="form-control"
                value={this.state.username}
                onChange={this.onChangeUsername}>
            </input>
            </div> 
            <div className="input-group">
              <label htmlFor="password">Password</label>
              <input
                type="password"
                name="password"
                className="login-input"
                value={this.state.password}
                onChange={this.onPassword}
                placeholder="Password"/>
            </div> 
            <div className="form-group">
            <label>education: </label>
            <input 
                type="text" 
                className="form-control"
                value={this.state.education}
                onChange={this.onEducation}
                />
            </div>
            <Button
              type="Button" value="Register" color="primary" variant="outlined" onClick={this.onSubmit.bind(this)}
              >Register</Button>
          </div>
        </div>
      );
    }
  }