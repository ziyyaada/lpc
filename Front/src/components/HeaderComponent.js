import React, {Component} from 'react';
import { Navbar, NavbarBrand, Nav, NavbarToggler, Collapse, NavItem, Jumbotron,
    Button, Modal, ModalHeader, ModalBody,
    Form, FormGroup, Input, Label } from 'reactstrap';
import {NavLink} from 'react-router-dom'; //attaches a tag and actor

class Header extends Component {
    constructor(props) {
        super(props);
        //making the toggleNav available as this
        this.toggleNav = this.toggleNav.bind(this);
        //bind the function
        this.toggleModal = this.toggleModal.bind(this);

        this.handleLogin = this.handleLogin.bind(this);
        this.setSearch = this.setSearch.bind(this);
        this.setLPSearch = this.setLPSearch.bind(this);
        this.state = {
            isNavOpen: false,
            isModalOpen: false //set as not open. will track the modal
        };
      }
//must bind this in constructor to use it
      toggleNav() {
        this.setState({
          isNavOpen: !this.state.isNavOpen
        });
      }
      //track the state of the modal
      toggleModal() {
        this.setState({
          isModalOpen: !this.state.isModalOpen
        });
      } 
      //what happens when login button pushed
      handleLogin(event) {
        this.toggleModal();//closes modal
        //retriecing values from the dom and using here
        alert("Username: " + this.username.value + " Password: " + this.password.value
            + " Remember: " + this.remember.checked);
        event.preventDefault(); //do not open new screen

    } 
     setLPSearch(){
        //alert('Hello');
       // var lpsearch =document.getElementById("search").value;
       var search = "";
        this.props.onClickLPSearch(search);
        
        //alert(search);
        
    }  

    setSearch(){
        //alert('Hello');
        var search ="";//document.getElementById("search").value"";
        this.props.onClickSearch(search);
        
        //alert(search);
        
    } 

    render() {
        return(
            <div>
                <Navbar dark expand="md fixed-top">
                    <div className="container">
                        <NavbarToggler onClick={this.toggleNav} />
                        <NavbarBrand className="mr-auto" href="/"><img src='' /></NavbarBrand>
                        <Collapse isOpen={this.state.isNavOpen} navbar>
                            <Nav navbar>
                            <NavItem>
                                <NavLink className="nav-link"  to='/home'><span className="fa fa-home fa-lg"></span> Home</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink className="nav-link"  to='/resources' onClick={()=>
                            this.setSearch()}><span className="fa fa-list fa-lg"></span> Resources</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink className="nav-link" to='/learningpaths' onClick={()=>
                            this.setLPSearch()}><span className="fa fa-info fa-lg"></span> Learning Paths</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink className="nav-link" to='/contactus'><span className="fa fa-address-card fa-lg"></span> Contact Us</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink className="nav-link"  to='/resources/create' ><span className="fa fa-list fa-lg"></span>Upload</NavLink>
                            </NavItem>
                            </Nav>
                            <Nav className="ml-auto" navbar>
                          
                                    {/* adding a button to be clicked */}
                                    <NavLink outline className="nav-link" to='/Login'><span className="fa fa-sign-in fa-lg"></span> Login</NavLink>

                            </Nav>
                        </Collapse>
                    </div>
                </Navbar>
                <Jumbotron>
                    <div className="container">
                        <div className="row row-header">
                            <div className="col-12 col-sm-6">
                                <h1>Learn Python Catalogue</h1>
                                <p>We take the the hard task of locating the right resources out of the equation!</p>
                            </div>
                        </div>
                    </div>
                </Jumbotron>

                {/* ------------------------Modal Start------------------------ */}
                
                <Modal isOpen={this.state.isModalOpen} toggle={this.toggleModal}>
                    {/* title */}
                    <ModalHeader toggle={this.toggleModal}>Login</ModalHeader> 
                        <ModalBody>
                            {/* handles the submittting of the login */}
                         <Form onSubmit={this.handleLogin}>
                            <FormGroup>
                                <Label htmlFor="username">Username</Label>
                                <Input type="text" id="username" name="username"
                                //store the user name in innerRef
                                //returns input where username is input
                                    innerRef={(input) => this.username = input} />
                            </FormGroup>
                            <FormGroup>
                                <Label htmlFor="password">Password</Label>
                                <Input type="password" id="password" name="password"
                                    innerRef={(input) => this.password = input}  />
                            </FormGroup>
                            <FormGroup check>
                                <Label check>
                                    <Input type="checkbox" name="remember"
                                    innerRef={(input) => this.remember = input}  />
                                    Remember me
                                </Label>
                            </FormGroup>
                            <Button type="submit" value="submit" color="primary">Login</Button>
                        </Form>
                    </ModalBody>
                </Modal>
            </div>
        );
    }
}

export default Header;