import React from 'react';
import {Card, CardImg, CardText, CardBody, CardTitle, CardSubtitle, Button} from 'reactstrap'
import {Link} from 'react-router-dom';
import {Loading} from './LoadingComponent'

function RenderCard({item, isLoading, errMess}) {
    if(isLoading){
        return(
            <Loading/>
        );
    }
    else if(errMess){
        return(
            <h4>{errMess}</h4>
        );
    }
    else {
        return(
        // <Card>
        //     <CardImg src={item.image} alt={item.name} />
        //     <CardBody>
        //     <CardTitle>{item.name}</CardTitle>
        //     {/* if item designation exists will return desc or null */}
        //     {item.designation ? <CardSubtitle>{item.designation}</CardSubtitle> : null }
        //     <CardText>{item.description}</CardText>
        //     </CardBody>
        // </Card>
        <div></div>
     );
     }
}



 


    function Home(props) {
        
        function setSearch(){
            //alert('Hello');
            var search =document.getElementById("search").value;
            props.onClick(search);
            
            //alert(search);
            
        }

        return(
// search bar here


            
            <div className="container">
            <br></br>
           {/* Search Bar Here */}
                <div className = "col-12 col-md-12">
                    <form>
                        <div class="form-group row">
                            <div class="col-md-10">
                                <input type ="text" class="form-control" id="search" name="search" placeholder="Search"/>
                            </div>
                            < Link to='/resources' className="col-md-2">
                            <button className="btn btn-success col-form-button col-md-12 " onClick={()=>
                            setSearch()}>Search</button>
                            </Link>
                            {/* <a role ="button" id="searchBut" class="btn btn-success col-md-2 col-form-button">Search</a> */}
                        </div>
                     </form>
                     <hr/>

                </div>
            {/* search bar ends */}
                
                <div className="row align-items-start">
                    <div className="col-12 col-md m-1">
                        <RenderCard item={props.resource} 
                        isLoading={props.resourcesLoading}
                        errMess={props.resourcesErrMess} />
                    </div>
                    <div className="col-12 col-md m-1">
                        {/* <RenderCard item={props.promotion} /> */}
                    </div>
                    <div className="col-12 col-md m-1">
                        <RenderCard item={props.leader} />
                    </div>
                </div>
            </div>
        );
    }

export default Home;