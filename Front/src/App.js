

//import navbar & NavbarBrand component from reactstrap
import {Form, Navbar, NavbarBrand} from 'reactstrap';
//import the reourceList comonent to be rendered to the UI
import ResourceList from './components/ResourceListComponent'; //now available
import './App.css';//using css file
import React, { Component } from 'react';
import {BrowserRouter} from 'react-router-dom';

import { render } from '@testing-library/react';
import Main from './components/MainComponent';
import {Provider} from 'react-redux';//allows to configure react app so store available to all 
import { ConfigureStore } from './redux/configureStore';

//define store to make it available
const store = ConfigureStore();

//displaying a page
class App extends Component {
//surround react application with Provider that takes the store attribute
//will supply store created above
  
  render(){
  return (
    // div might not be needed here
   // <div className= "App">  
   // <Provider store={store}>
    <BrowserRouter>
      <Main/>
    </BrowserRouter> 
   // </Provider>
  //  </div>
  );
  }
}

//exporting this so can be used in other files
export default App;
