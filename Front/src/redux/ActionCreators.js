// //import everythuing that is being exported by the action types file

// import * as ActionTypes from './ActionTypes';
// //function that creates action object.
// //takes 4 parameters as data that will be included by acrion object
// //will return object that we create
// export const addComment = (resourceId, rating, author, comment) =>({
//     //first thing is the type. all action objects mst contain this
//     //use action types file to get these
//     type: ActionTypes.ADD_COMMENT,
//     // now need data put as payload. this is data being transfered to reducer object
//     payload: {
//         // will contain all resource information that is beinng passed
//         //mapping the parameters received being mapped to the payload
//         //now ready to be returned to the store. 
//         //should just be changing comments part of the state in comments.js
//         resourceId: resourceId,
//         rating: rating,
//         author: author,
//         comment: comment
//     }
// });

import * as ActionTypes from './ActionTypes';
// import {RESOURCES} from '../shared/resources';//import the object resources from resource file
import {baseUrl} from '../shared/baseUrl';

//---------------- COMMENTS----------------------------

//returns the comments
export const fetchComments = () =>(dispatch) =>{
    //do not need to set up load here
       //fetch from server
        return fetch(baseUrl + 'comments')
        .then(response =>{
            //.ok means response was good
                if (response.ok){
                    //send this to the next then
                    return response;
                }
                //HANDLE ERROR. response status gives error code
                else{
                    var error =new Error('Error ' + response.status
                    + ': ' + response.statusText);
                    error.response =response;
                    throw error;//through error here then gets handled at botton
                }
        },
        error=> {
            var errmess = new Error(error.message);
            throw errmess;
        })
            //take response and turn into json
            .then(response => response.json())
            //will call respose as a comemtns and will add it to
            //the redux store that we have
            //add Comments will add to our redux store
            .then(comments => dispatch(addComments(comments)))
        //then does this..push reousrces into the state
            .catch(error => dispatch(commentsFailed(error.message)));//error causes rejected promise

        
};

export const commentsFailed = (errmess)=>({
    type: ActionTypes.COMMENTS_FAILED,
    //whatever we pass into this payload
    payload: errmess
});
//fn will return an action. will take all resources 
//will add to resources
export const addComments = (comments) =>({
    type: ActionTypes.ADD_COMMENTS,
    payload: comments

});
//------------posting a comment to server------------
export const addComment = (comment) => ({
    type: ActionTypes.ADD_COMMENT,
    payload: comment
});

export const postComment = (resourceId, rating, author, comment) => (dispatch) => {
    //create new object and map parameters
    const newComment = {
        resourceId: resourceId,
        rating: rating,
        author: author,
        comment: comment
    };
    //add date here
    newComment.date = new Date().toISOString();
    //fect hfrom the server
    return fetch(baseUrl + 'comments', {
        //if do not specify method then default to get
        //make method
        method: "POST",
        body: JSON.stringify(newComment),//turn into jason and put in body
        headers: {//create the headers for the server
          "Content-Type": "application/json" //specify datatypes
        },
        credentials: "same-origin" //
    })
    //get response from server and handle errors
    .then(response => {
        if (response.ok) {
          return response;
        } else {
          var error = new Error('Error ' + response.status + ': ' + response.statusText);
          error.response = response;
          throw error;
        }
      },
      error => {
            throw error;
      })
    .then(response => response.json())
    .then(response => dispatch(addComment(response)))//put in redux store
    .catch(error =>  { console.log('Post comments', error.message); alert('Your comment could not be posted\nError: '+error.message); });
};

//----------------------COMMENTS END ----------------

// ---------------------RESOURCES--------------------
//return the dispatch fn. inner function gets access 
//fetch resources returns a fn
export const fetchResources = () =>(dispatch) =>{
    //First does this dispatch
        dispatch(resourcesLoading(true));

        //communicate with the server with FETCH
        return fetch(baseUrl + 'resources')
        //handle errors and data being returned
            .then(response =>{
                //.ok means response was good
                    if (response.ok){
                        //send this to the next then
                        return response;
                    }
                    //HANDLE ERROR. response status gives error code
                    else{
                        var error =new Error('Error ' + response.status
                        + ': ' + response.statusText);
                        error.response =response;
                        throw error;//through error here then gets handled at botton
                    }
            },
            error=> {
                var errmess = new Error(error.message);
                throw errmess;
            })
            //take response and turn into json
            .then(response => response.json())
            //will call respose as a resuorce and will add it to
            //the redux store that we have
            .then(resources => dispatch(addResources(resources)))
        //then does this..push reousrces into the state
            .catch(error => dispatch(resourcesFailed(error.message)));//error causes rejected promise
}

export const resourcesLoading = () => ({
    //will return an action
    //will inform someonne that resources being loaded
    type: ActionTypes.RESOURCES_LOADING
});

export const resourcesFailed = (errmess)=>({
    type: ActionTypes.RESOURCES_FAILED,
    //whatever we pass into this payload
    payload: errmess
});
//fn will return an action. will take all resources 
//will add to resources
export const addResources = (resources) =>({
    type: ActionTypes.ADD_RESOURCES,
    payload: resources

});

//------------------POST RESOURCE TO SERVER------------
//READY TO IMPLEMENT

// export const addResource = (resource) => ({
//     type: ActionTypes.ADD_RESOURCE,
//     payload: resource
// });
// //define resource params here
// export const postResource = (resourceId, rating, author, comment) => (dispatch) => {
//     //create new object and map parameters
//     const newResource = {
//         resourceId: resourceId,
//         rating: rating,
//         author: author,
//         comment: comment
//     };
//     //add date here
//     newResource.date = new Date().toISOString();
//     //fect hfrom the server
//     return fetch(baseUrl + 'resources', {
//         //if do not specify method then default to get
//         //make method
//         method: "POST",
//         body: JSON.stringify(newResource),//turn into jason and put in body
//         headers: {//create the headers for the server
//           "Content-Type": "application/json" //specify datatypes
//         },
//         credentials: "same-origin" //
//     })
//     //get response from server and handle errors
//     .then(response => {
//         if (response.ok) {
//           return response;
//         } else {
//           var error = new Error('Error ' + response.status + ': ' + response.statusText);
//           error.response = response;
//           throw error;
//         }
//       },
//       error => {
//             throw error;
//       })
//     .then(response => response.json())
//     .then(response => dispatch(addResource(response)))//put in redux store
//     .catch(error =>  { console.log('Post resources', error.message); alert('Your resource could not be posted\nError: '+error.message); });
// };

//------------------RESOURCES END---------------

//------------------LEARNING PATH START---------

export const fetchLearningpaths = () =>(dispatch) =>{
    //First does this dispatch
        dispatch(learningpathLoading(true));

        //communicate with the server with FETCH
        return fetch(baseUrl + 'learningpaths')

        .then(response =>{
            //.ok means response was good
                if (response.ok){
                    //send this to the next then
                    return response;
                }
                //HANDLE ERROR. response status gives error code
                else{
                    var error =new Error('Error ' + response.status
                    + ': ' + response.statusText);
                    error.response =response;
                    throw error;//through error here then gets handled at botton
                }
        },
        error=> {
            var errmess = new Error(error.message);
            throw errmess;
        })
            //take response and turn into json
            .then(response => response.json())
            //will call respose as a resuorce and will add it to
            //the redux store that we have
            .then(learningpaths => dispatch(addLearningpaths(learningpaths)))
        //then does this..push reousrces into the state
             .catch(error => dispatch(learningpathsFailed(error.message)));//error causes rejected promise

}

export const learningpathLoading = () => ({
    //will return an action
    //will inform someonne that resources being loaded
    type: ActionTypes.LEARNINGPATHS_LOADING
});

export const learningpathsFailed = (errmess)=>({
    type: ActionTypes.LEARNINGPATHS_FAILED,
    //whatever we pass into this payload
    payload: errmess
});
//fn will return an action. will take all resources 
//will add to resources
export const addLearningpaths = (learningpaths) =>({
    type: ActionTypes.ADD_LEARNINGPATHS,
    payload: learningpaths

});

//------------POSTING LEARNINGPATH TO SERVER------------

//READY FOR USE
// export const addLearningpath = (learningpath) => ({
//     type: ActionTypes.ADD_LEARNINGPATH,
//     payload: learningpath
// });

// export const postLearningpath = (resourceId, rating, author, comment) => (dispatch) => {
//     //create new object and map parameters
//     const newLearningpath = {
//         resourceId: resourceId,
//         rating: rating,
//         author: author,
//         comment: comment
//     };
//     //add date here
//     newLearningpath.date = new Date().toISOString();
//     //fect hfrom the server
//     return fetch(baseUrl + 'comments', {
//         //if do not specify method then default to get
//         //make method
//         method: "POST",
//         body: JSON.stringify(newLearningpath),//turn into jason and put in body
//         headers: {//create the headers for the server
//           "Content-Type": "application/json" //specify datatypes
//         },
//         credentials: "same-origin" //
//     })
//     //get response from server and handle errors
//     .then(response => {
//         if (response.ok) {
//           return response;
//         } else {
//           var error = new Error('Error ' + response.status + ': ' + response.statusText);
//           error.response = response;
//           throw error;
//         }
//       },
//       error => {
//             throw error;
//       })
//     .then(response => response.json())
//     .then(response => dispatch(addLearningpath(response)))//put in redux store
//     .catch(error =>  { console.log('Post learning path', error.message); alert('Your learning path could not be posted\nError: '+error.message); });
// };



//------------------ LEARNING PATH END ---------