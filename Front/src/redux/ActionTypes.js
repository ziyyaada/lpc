//standardized template

//will export multiple actions to imported into reducer that will go through switch statement
//define action types
export const ADD_COMMENT = 'ADD_COMMENT'; //DONE ALREADY
export const ADD_COMMENTS = 'ADD_COMMENTS';
export const COMMENTS_FAILED = 'COMMENTS_FAILED';

export const RESOURCES_LOADING = 'RESOURCES_LOADING';
export const RESOURCES_FAILED = 'RESOURCES_FAILED';
export const ADD_RESOURCES = 'ADD_RESOURCES'; 
export const ADD_RESOURCE = 'ADD_RESOURCE'; 

export const LEARNINGPATHS_LOADING = 'LEARNINGPATHS_LOADING';
export const LEARNINGPATHS_FAILED = 'LEARNINGPATHS_FAILED';
export const ADD_LEARNINGPATHS = 'ADD_LEARNINGPATHS'; 
export const ADD_LEARNINGPATH = 'ADD_LEARNINGPATH'; 

//COMMENTS NOT NEEDED AS DONE IN HOME COMPONENT AND SO WONT NEED TO BE LOADED