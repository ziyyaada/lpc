import {createStore, combineReducers, applyMiddleware} from 'redux';//combine reducers as single
import { Resources } from './resources'
import { Comments } from './comments'
import { Learningpaths } from './learningpaths'
import thunk from 'redux-thunk';
import logger from 'redux-logger';


export const ConfigureStore = () => {
    const store = createStore(
       //create store out of combined reducers
       //import the data needed and recompose global state
       combineReducers({
            resources: Resources,
            comments: Comments,
            learningpaths: Learningpaths
       }),
       applyMiddleware(thunk, logger)//new parameter
       //thunk will be modified to work with serer

    );

    return store;
}