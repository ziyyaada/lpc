import * as ActionTypes from './ActionTypes';

//give default value as resources that will be retruned as default
//state is set to 3 parameters: isLoading, errMess and resources
export const Resources = (state = {

    isLoading: true, //initially resources is empty and isLoading is true.will be false when resources loaded
    errMess: null, //set to err message received from server or null
    resources: [] // will load resources in here from add resources action
}, action)=>{
switch(action.type) {
    //must interpret what the actions mean
    case ActionTypes.ADD_RESOURCES:
    //if return reources. Payload of action object will be set to reoucres
         return{...state, isLoading: false, errMess: null, resources: action.payload};//will take same state and apply change

    case ActionTypes.RESOURCES_LOADING:
        //why need errmess and reousrces? 
         return{...state, isLoading: true, errMess: null, resources:[]};//will take same state and apply change
    case ActionTypes.RESOURCES_FAILED:
        //payload will contain err message. Tried to load but couldnt
        // when retrive info can display in view what state contains
         return{...state, isLoading: false, errMess: action.payload, resources:[]};//will take same state and apply change
    case ActionTypes.ADD_RESOURCE:
         //add comment into redux store
            var resource = action.payload;
            //comment.id = state.comments.length;
            //comment.date = new Date().toISOString();
            console.log("Resource: ", resource);
            return {...state, resources: state.resources.concat(resource)};
    default: 
        return state;//returns the state un,modified

}

}