// import {LEARNINGPATHS} from '../shared/learningpaths';
import * as ActionTypes from './ActionTypes';


//give default value as resources that will be retruned as default
export const Learningpaths = (state = {

        isLoading: true, //initially resources is empty and isLoading is true.will be false when resources loaded
        errMess: null, //set to err message received from server or null
        learningpaths: [] // will load resources in here from add dishes action
    }, action)=>{
    switch(action.type) {
 //must interpret what the actions mean
 case ActionTypes.ADD_LEARNINGPATHS:
    //if return reources. Payload of action object will be set to reoucres
         return{...state, isLoading: false, errMess: null, learningpaths: action.payload};//will take same state and apply change

    case ActionTypes.LEARNINGPATHS_LOADING:
        //why need errmess and reousrces? 
         return{...state, isLoading: true, errMess: null, learningpaths:[]};//will take same state and apply change
    case ActionTypes.LEARNINGPATHS_FAILED:
        //payload will contain err message. Tried to load but couldnt
        // when retrive info can display in view what state contains
         return{...state, isLoading: false, errMess: action.payload, learningpaths:[]};//will take same state and apply change
    case ActionTypes.ADD_LEARNINGPATH:
         //add comment into redux store
            var learningpath = action.payload;
            //comment.id = state.comments.length;
            //comment.date = new Date().toISOString();
            console.log("Learning Path: ", learningpath);
            return {...state, learningpaths: state.learningpaths.concat(learningpath)};
        default: 
            return state;//returns the state un,modified
    }
    
    }