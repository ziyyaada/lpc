// import {COMMENTS} from '../shared/comments';
// //will use action types here
// import * as ActionTypes from './ActionTypes';


// //give default value as resources that will be retruned as default
// export const Comments = (state = COMMENTS, action)=>{
//     switch(action.type) {
//         //act on action type. If type matches the case, reducer ,ust do something to the state
//         //
//         case ActionTypes.ADD_COMMENT:
//             //payloasd is js object that has parts of comment
//             var comment = action.payload;
//             //id will come from server.
//             //comments is coming from comments so can use state.length
//             comment.id = state.length;
//             //need date. willget from server later. date stored as ISOString
//             comment.date = new Date().toISOString(); 
//             console.log("Comment ", comment)
//             //cannot modify state that is sent in, can add to it and 
//             //return modified version
//             //concat pushes element into the array that will be a 
//             //new object that can be returned
//             //this comment is only added in memory not persisted
//             return state.concat(comment); //will add comment to our set and return the value
//         default: 
//             return state;//returns the state un,modified
//     }
     
//     }

// import { COMMENTS } from '../shared/comments';
import * as ActionTypes from './ActionTypes';

export const Comments = (state = {
    errMess: null,
    comments: []
}, action) => {
    switch (action.type) {
            //must interpret what the actions mean
    case ActionTypes.ADD_COMMENTS:
        //if return reources. Payload of action object will be set to reoucres
        return{...state, errMess: null, comments: action.payload};//will take same state and apply change
       
     case ActionTypes.COMMENTS_FAILED:
            //payload will contain err message. Tried to load but couldnt
            // when retrive info can display in view what state contains
        return{...state, errMess: action.payload};//will take same state and apply change

     case ActionTypes.ADD_COMMENT:
         //add comment into redux store
            var comment = action.payload;
            //comment.id = state.comments.length;
            //comment.date = new Date().toISOString();
            console.log("Comment: ", comment);
            return {...state, comments: state.comments.concat(comment)};


        default:
          return state;
      }
};

